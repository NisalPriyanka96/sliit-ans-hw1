import socket

serverSocket = socket.socket()

# Bind and listen

host = socket.gethostname()
port = 1234
serverSocket.bind((host, port))

# maximum 2 user can connect simultaniously
serverSocket.listen(2)

## this variable is for store count for each client connections
count = 0

## char variable to store client1 receiving character
char = ''


while(True):

    # establshing the connection
    clientConnected, clientAddress = serverSocket.accept()

    print("Connection Accepted from %s:%s "%(clientAddress[0], clientAddress[1]))

    if count == 0:
        dataFromClient1 = clientConnected.recv(1024)
        temp_str = dataFromClient1.decode()
        #set the client1 send data
        char = temp_str[0]
        clientConnected.close()
    else :
        # Increment Char
        char = chr(ord(char) + 1)
        # Send data back to the client 2
        clientConnected.send(char.encode())
        clientConnected.close()
        break

    ## increment when each client connects
    count += 1