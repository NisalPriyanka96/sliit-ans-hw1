import socket

# Create a client socket

clientSocket = socket.socket()

# Connect to the server

clientSocket.connect((socket.gethostname(),1234))

# Send simple char value to server

SimpleChar = "A"
clientSocket.send(SimpleChar.encode())

# Print to the console
print('Client 1 has sent %s to the Server' % (SimpleChar))