import socket

# Create a client socket

clientSocket = socket.socket()

# Connect to the server

clientSocket.connect((socket.gethostname(),1234))

# Receive data from server

dataFromServer = clientSocket.recv(1024)


# Print to the console
print('Client 2 has received %s from the Server' % (dataFromServer.decode()))