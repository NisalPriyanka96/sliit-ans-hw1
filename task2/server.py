import socket

serverSocket = socket.socket()

host = socket.gethostname()
port = 1234

serverSocket.bind((host, port))

# allow max 2 users to connect to our server
serverSocket.listen(2)

count = 0
number = 0

while True:
    ClientConnection, ClientAddress = serverSocket.accept()
    print("Connection Accepted from %s:%s "%(ClientAddress[0],ClientAddress[1]))

    if count == 0:
        dataFromClient1 = ClientConnection.recv(1024).decode()
        number = int(dataFromClient1)
        ClientConnection.close()
    else:
        number = number-1
        ClientConnection.send(str(number).encode('ascii'))
        ClientConnection.close()
        break;

    count = count+1
    